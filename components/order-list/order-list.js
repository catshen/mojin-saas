
import { getOrderList, cancelOrder, delOrder, confirmOrder } from '../../api/user'
import { prepay } from '../../api/app'
import { loadingType } from '../../utils/type'
import { Tips } from '../../utils/util'
import event from '../../utils/events'
import { wxpay } from '../../utils/wxutil'
import Toast from '../../components/weapp/toast/toast';
Component({
	/**
	 * 组件的属性列表
	 */
	properties: {
		orderType: {
			type: String
		}
	},

	/**
	 * 组件的初始数据
	 */
	data: {
		page: 0,
		orderList: [],
		status: loadingType.LOADING,
		showCancel: false
	},
	lifetimes: {
		created: function () {
			event.on('RESET_LIST', this.reflesh, this)
		},
		attached: function () {
			this.$getOrderList()
		},
		detached: function () {
			// 在组件实例被从页面节点树移除时执行
			event.remove('RESET_LIST')
		},
	},
	pageLifetimes: {
		// onReachBottom:function () {
		// 	this.$getOrderList()
		// }
	},
	/**
	 * 组件的方法列表
	 */
	methods: {
		reflesh() {
			this.data.page = 0,
			this.data.orderList = [],
			this.data.status = loadingType.LOADING
			this.setData({
				type: 0,
			})
			this.$getOrderList()
		},
		reload() {
			this.setData({
				status: loadingType.LOADING
			})
			this.$getOrderList()
		},
		goPage(e) {
			let { url } = e.currentTarget.dataset
			wx.navigateTo({
				url
			})
		},
		onShowDialog() {
			let { showCancel } = this.data
			this.setData({
				showCancel: !showCancel
			})
		},
		async onConfirm() {
			const { type } = this.data
			let res = null
			switch(type) {
				case 0 : res = await cancelOrder(this.id) 
				break;
				case 1 : res = await delOrder(this.id) 
				break;
				case 2 : res = await confirmOrder(this.id) 
				break;
			}
			if (res.code == 200) {
				this.onShowDialog()
				Tips({ title: res.msg })
				event.emit('RESET_LIST')
			}
		},
		delOrder(e) {
			this.id = e.currentTarget.dataset.id
			this.setData({
				type: 1
			})
			wx.nextTick(() => {
				this.onShowDialog()
			})

		},
		comfirmOrder(e) {
			this.id = e.currentTarget.dataset.id
			this.setData({
				type: 2
			})
			wx.nextTick(() => {
				this.onShowDialog()
			})

		},
		cancelOrder(e) {
			this.id = e.currentTarget.dataset.id
			this.setData({
				type: 0
			})
			wx.nextTick(() => {
				this.onShowDialog()
			})
		},
		payNow(e) {
			this.toast = Toast.loading({
				duration: 0, // 持续展示 toast
				forbidClick: true,
				message: '请稍等...',
			});
			let { id } = e.currentTarget.dataset
			prepay({
				type: 1,
                     orderCode:id
			}).then(res => {
				if (res.code == 200) {
				 var r = res.data
                            if(r.flag==-5){
                            Tips({ title: '微信生成订单出错' })

                            					return false;
                            				}
                            				if(r.flag==-3){
                            				Tips({ title: '没有待支付订单' })
                            					return false;
                            				}
                            				if(r.flag==-1){
                                                        				Tips({ title: '用户不存在' })
                                                        					return false;
                                                        				}
                            				if(r.flag==-7){
                                                        				Tips({ title: '没有设置网站地址' })
                                                        					return false;
                                                        				}

                            				if(r.flag==-8){
                            				Tips({ title: '参数错误' })
                            					return false;
                            				}
					Toast.clear()
					let args = res.data.data
					wxpay(args).then(()=> {
						Tips({ title: '支付成功' })
						event.emit('RESET_LIST')
					}).catch(() => {
						
					})
				}
			})
		},
		$getOrderList() {
			let { page, orderType, orderList, status } = this.data
			if (status == loadingType.FINISHED) return
			console.log(orderType)
			var odstatus = -1;
			if(orderType=='pay'){
			    odstatus = 1;
			}else if(orderType=='delivery'){
                  			    odstatus = 2;
                  			}if(orderType=='deliveryed'){
                             			    odstatus = 3;
                             			}if(orderType=='finish'){
                                         			    odstatus = 4;
                                         			}if(orderType=='close'){
                                                     			    odstatus = 5;
                                                     			}
			getOrderList({
				status: odstatus,
				pageNum: page
			}).then(res => {
				if (res.code == 200) {
					let { list,totalPages } = res.data
					orderList.push(...list)
					this.setData({
						orderList,
						page: ++page
					})
					wx.nextTick(() => {
						if (totalPages<=page) {
							this.setData({
								status: loadingType.FINISHED
							})
						}
						if (orderList.length <= 0) {
							this.setData({
								status: loadingType.EMPTY
							})
							return
						}
					})
				} else {
					this.setData({
						status: loadingType.ERROR
					})
				}
			})
		}
	}
})
