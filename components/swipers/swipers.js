
import {getAdList} from '../../api/store'
var app = getApp();
Component({
  properties: {
    pid: {
      type: Number
    },
    height: {
      type: String,
    },
    radius: {
      type: String,
      default: '10rpx'
    },
    padding: {
      type: String,
      default: '0rpx'
    }
  },
  data: {
    lists: [],
    circular: true,
    autoplay: true,
    interval: 3000,
    duration: 500,
    currentSwiper: 0
  },
  observers: {
    'pid': function(value) {
      this.$getAdList()
    }
  },
  methods: {
    $getAdList() {
      getAdList({pid: this.properties.pid}).then(res => {
        if(res.code == 200) {
        console.log(res.data)
          this.setData({
            lists: res.data
          })
        }
      })
    },
    swiperChange: function (e) {
      this.setData({
        currentSwiper: e.detail.current
      })
    },
    goPage: function(e) {
      let {item} = e.currentTarget.dataset;
      let {link, link_type, params, is_tab} = item;
      let args = "?"
      if(params instanceof Array) {
        // 若判断为数组
        console.log(params, "$$$")
      }
      else if(params instanceof Object) {
        // 非数组，但为对象
        console.log(params, '###')
        Object.keys(params).forEach((keys) => {
          args += keys + '=' + params[keys] + '&'
        })
      }

      switch (1) {
        case 1:
         wx.navigateTo({
                      url: item.url,
                    })
                    break;
        case 2:
          if(is_tab) {
            wx.switchTab({
              url: link + args,
            })
          }
          else {
            wx.navigateTo({
              url: link + args,
            })
          }
          break;
        case 3:
            wx.navigateTo({
              url: '/pages/webview/webview?url=' + link,
            })
            break;
      }
    }
  }
})