
import { loadingType } from '../../utils/type'
import {getOrderCommentList} from "../../api/store"
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    type: {
      type: Number,
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    list: [],
    status: loadingType.LOADING,
    page: 0
  },

  lifetimes: {
    attached() {
      console.log( this.data.type)
      this.$getOrderCommentList()
    }
  },

  /**
   * 组件的方法列表
   */
  methods: {
    $getOrderCommentList() {
      let {page, type, status, list} = this.data;
      if(status == loadingType.FINISHED) return;
      getOrderCommentList({pageNum: page, status: type}).then(res => {
          if(res.code == 200) {
            let {list: lists, totalPages} = res.data;
            list.push(...lists);
            this.setData({
              list: list,
              page: ++page,
            })
            if(totalPages<=page) {
              this.setData({
                status: loadingType.FINISHED
              })
            }
            console.log('ccc')
            console.log(this.data.list)
            if(list.length <= 0) {
              this.setData({
                status: loadingType.EMPTY
              })
            }
          }
      })
    },
     goToDetail(e) {

                let { id } = e.currentTarget.dataset
                wx.navigateTo({
                    url: `/pages/product/product?id=${id}`
                })
            },
    goPage(e) {
      let {url} = e.currentTarget.dataset;

        wx.navigateTo({
          url: url,
        })

    }
  }
})
