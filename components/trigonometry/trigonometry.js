
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    color: {
      type: String,
      default: ''
    },
    direction: {
      type: String
    },
    size: {
      type: String
    },
    opacity: {
      type: String,
      default: '0.8'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {

  }
})
