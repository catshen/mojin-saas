
import {getBestList} from '../../api/store'
Component({
    /**
     * 组件的属性列表
     */
    properties: {

    },

    /**
     * 组件的初始数据
     */
    data: {
        goodsList: []
    },
    lifetimes: {
        attached: function() {
            this.$getBestList()
        },
        detached: function() {
          // 在组件实例被从页面节点树移除时执行
        },
      },
    /**
     * 组件的方法列表
     */
    methods: {
        $getBestList() {
            getBestList({
                pageNum: 0,
                page_size: 6
            }).then(res => {
                if(res.code == 200) {
                    this.setData({
                        goodsList: res.data.list
                    })
                }
            })
        }
    }
})
