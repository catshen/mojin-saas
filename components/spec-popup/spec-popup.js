
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        show: {
            type: Boolean
        },
        goods: {
            type: Object
        },
        showAdd: {
            type: Boolean,
            value: true
        },
        showBuy: {
            type: Boolean,
            value: true
        },
        showBargain: {
            type: Boolean,
            value: false
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        checkedGoods: {},
        specList: [],
        goodsNum: 1
    },

    /**
     * 组件的方法列表
     */
    methods: {
        onClose() {
            this.triggerEvent('close')
        },
        onClick(e) {
            const {type } = e.currentTarget.dataset
            let {checkedGoods, goodsNum} = this.data
            console.log(checkedGoods, goodsNum)
            checkedGoods.goodsNum = goodsNum
            this.triggerEvent(type, checkedGoods)
        },
        onChange(e) {
            this.setData({
                goodsNum: e.detail
            })
        },
        choseSpecItem(e) {
            const { id, specid } = e.currentTarget.dataset

            let { specList } = this.data
            specList.forEach(item => {
                if (item.spec.goodsSpecValues && item.spec.id == id) {
                    item.spec.goodsSpecValues.forEach(specitem => {
                        specitem.checked = 0
                        if (specitem.id == specid) {
                            specitem.checked = 1
                        }
                    })
                }
            })

            this.setData({
                specList
            })
        },
        previewImage(e) {
            const {current} = e.currentTarget.dataset
            wx.previewImage({
                current, // 当前显示图片的http链接
                urls: [current] // 需要预览的图片http链接列表
              })
        }
    },
    observers: {
        goods(value) {
            let specList = value.skuSpecValues || []
            let goodsItem = value.skuList || []
            specList.forEach(item => {
                if (item.spec.goodsSpecValues) {
                    item.spec.goodsSpecValues.forEach((specitem, specindex) => {
                        if (specindex == 0) {
                            specitem.checked = 1
                        } else {
                            specitem.checked = 0
                        }
                    })
                }
            })
            
            this.setData({
                specList,
                checkedGoods: goodsItem.length ? goodsItem[0] : {}
            })
        },
        
        specList(value) {
            const { skuList } = this.data.goods
            let keyArr = []

            value.forEach((item) => {
                if (item.spec.goodsSpecValues) {
                    item.spec.goodsSpecValues.forEach((specitem) => {
                        if (specitem.checked) {
                            keyArr.push(specitem.name)
                        }
                    })
                }
            })
            
            if (!keyArr.length) return
            let key = keyArr.join('-')

            let index = skuList.findIndex(item => {
                return item.subtitle == key
            })
            if(index == -1) return
            this.setData({
                checkedGoods: skuList[index]
            })
            console.log(skuList[index])
            this.triggerEvent('change', skuList[index])
        }
    },

})
