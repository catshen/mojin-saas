
Component({
	/**
	 * 组件的属性列表
	 */
    options: {
        multipleSlots: true // 在组件定义时的选项中启用多slot支持
    },
    properties: {
        height: {
            type: String,
            value: '100vh'
        },
        status: {
            type: String,
            value: 'loading'
        },
        finishedText: {
            type: String,
            value: '我可是有底线的～'
        },
        errorText: {
            type: String,
            value: '加载失败，点击重新加载'
        },
        slotFooter: {
            type: Boolean,
            value: false
        }
    },
	/**
	 * 组件的初始数据
	 */
    data: {
        
    },
    lifetimes: {
        ready: function () {
            // this.getScroolHeight()
        }
    },
	/**
	 * 组件的方法列表
	 */
    methods: {
        toLower() {
            if (this.data.status === 'loading') {
                this.triggerEvent('scrolltobottom')
            }
        },
        // getConHeight() {
        //     this.createSelectorQuery().select('.content').boundingClientRect((rect) => {
        //         if (rect && this.scroolHeight  >= rect.height) {
        //             this.toLower()
        //         }
        //     }).exec()
        // },
        // getScroolHeight() {
        //     this.createSelectorQuery().select('.scroll-wrap').boundingClientRect((rect) => {
        //         this.scroolHeight = rect && rect.height
        //     }).exec()
        // }
    },
    observers: {
        'status': function (val) {
            // wx.nextTick(() => {
            //     this.getConHeight()
            // })
        }
    }
})
