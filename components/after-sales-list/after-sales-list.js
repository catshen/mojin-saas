
import {AfterSaleType, loadingType} from "../../utils/type"
import {getAfterSaleList, cancelApply, afterSaleDetail, applyAgain} from "../../api/user"
import event from '../../utils/events'
import { Tips } from "../../utils/util"
Component({
  properties: {
    type: {
      type: String,
      value: AfterSaleType.all
    }
  },

  data: {
    lists: [],
    status: [
           		{key: 1, value: '退款申请'},
           		{key: 2, value: '退款成功'},
           		{key: 3, value: '退款拒绝'},
           		{key: 4, value: '退货申请'},
           		{key: 5, value: '退货拒绝'},
           		{key: 6, value: '退货审核通过等待用户填写物流'},
           		{key: 7, value: '待收货 '},
           		{key: 8, value: '退货完成'},
           		{key: 9, value: '退货失败（商家不同意退款）'},
           		{key: 10, value: '已退钱'}
           	],
    page: 0,
    loadingStatus: loadingType.LOADING,
    confirmDialog: false,
  },

  methods: {
    $cancelApply() {
      cancelApply({id: this.id}).then(res => {
        if(res.code == 200) {
          Tips({title: res.msg});
          event.emit('RESET_LIST');
        }
      })
    },
    $getAfterSaleList() {
      let {lists, loadingStatus, page} = this.data;
      if(loadingStatus == loadingType.FINISHED) return;

      var odstatus = 0;
      			if(this.properties.type=='refundApply'){
      			    odstatus = 1;
      			}else if(this.properties.type=='refundSucess'){
                        			    odstatus = 2;
                        			}if(this.properties.type=='refundReApply'){
                                   			    odstatus = 4;
                                   			}if(this.properties.type=='refundReSucess'){
                                               			    odstatus = 8;
                                               			}
      getAfterSaleList({status: odstatus, pageNum: page}).then(res => {
        if(res.code == 200) {
          let {list, totalPages} = res.data;
          lists.push(...list);
          this.setData({
            lists: lists,
            page: ++ page
          })
          if(totalPages<=page) {
            this.setData({
              loadingStatus: loadingType.FINISHED
            })
          }
          if(lists.length <= 0) {
            this.setData({
              loadingStatus: loadingType.EMPTY
            })
          }
          return;
        } else {
          this.setData({
            loadingStatus: loadingType.ERROR
          })
        }
      })
    },
    goPage(e) {
      let {url, orderId, item_id} = e.currentTarget.dataset;
      url = url + '?orderId=' + orderId + '&item_id=' + item_id
      wx.navigateTo({
        url: url,
      })
    },
    goToDetail(e) {

                let { id } = e.currentTarget.dataset
                wx.navigateTo({
                    url: `/pages/product/product?id=${id}`
                })
            },
    reflesh() {
      this.data.page = 0;
      this.data.lists = [];
      this.data.loadingStatus = loadingType.LOADING;
      this.$getAfterSaleList();
    },

    showDialog(e) {
      let {id} = e.currentTarget.dataset
      this.id = id
      this.setData({
        confirmDialog: true
      })
    },
  
    hideDialog() {
      this.setData({
        confirmDialog: false,
      })
    },
  },
  lifetimes: {
    created() {
      event.on('RESET_LIST', this.reflesh, this)
    },
    attached() {
      this.$getAfterSaleList()
    },
  }
})