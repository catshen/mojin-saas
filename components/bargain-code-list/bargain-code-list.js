
import {bargainCodeType, loadingType} from "../../utils/type"

Component({
  properties: {
    type: {
      type: String,
      value: bargainCodeType.ALL
    }
  },

  data: {
    lists: [],
    page: 0,
    loadingStatus: loadingType.LOADING,
    confirmDialog: false,
  }
})