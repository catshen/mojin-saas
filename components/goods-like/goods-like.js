
import { getRect } from '../../utils/wxutil'
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        list: {
            type: Array,
            value: [],
            observer: function () {
                getRect(".goods-like", false, this).then(res => {
                    this.rectWidth = res.width
                });
            }
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        progressPer: 0
    },
  
    /**
     * 组件的方法列表
     */
    methods: {
        scrollBarChange(e) {
            let { progressPer } = this.data
            let { scrollLeft, scrollWidth } = e.detail;
            progressPer = (scrollLeft / (scrollWidth - this.rectWidth)) * 100;
            progressPer = progressPer.toFixed(0)
            this.setData({ progressPer: progressPer });
        },
    }
})
