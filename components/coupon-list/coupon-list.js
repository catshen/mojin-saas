
import { getCoupon } from '../../api/user'
import { Tips } from '../../utils/util'
Component({
    /**
     * 组件的属性列表
     */
    properties: {
        list: {
            type: Array,
            value: [],
            observer: function (val) {
                let arr = val.map(item => {
                    return 0
                })
                this.setData({
                    showTips: arr
                })
            }
        },
        btnType: {
            // 0 去使用  1已使用 2已过期 3领取
            type: Number
        }
    },

    /**
     * 组件的初始数据
     */
    data: {
        showTips: []
    },

    /**
     * 组件的方法列表
     */
    methods: {
        onHandle(e) {
            this.id = e.currentTarget.dataset.id
            const { btnType } = this.data
            switch (btnType) {
                case 0:
                    wx.switchTab({
                        url: '/pages/index/index',
                      })
                    break;
                case 1:
                    wx.switchTab({
                                            url: '/pages/index/index',
                                          })
                                        break;
                    break;
                case 2:
                    // text = '已使用';
                    break;
                case 4:
                    this.$getCoupon()
                    break;
            }

        },
        onShowTips(e) {
            const {showTips} = this.data
            const { index } =  e.currentTarget.dataset
            this.setData({
                [`showTips[${index}]`]: showTips[index] ? 0 : 1
            })
        },
        $getCoupon() {
            getCoupon(this.id).then(res => {
                if (res.code == 200) {
                var r= res.data;
                // 成功 -1：参数不全 -2：活动已过期 -3：优惠券已领完 -4：用户领取的优惠券已达上限 -5优惠券已失效(删除状态) -6 系统繁忙，请重试
                						if (r == 1) {

                							Tips({ title: '领取成功' })
                						} else if (r == -1) {

                							Tips({ title: '参数不全' })
                							return false;
                						} else if (r == -2) {

                							Tips({ title: '活动已过期' })
                							return false;
                						} else if (r == -3) {

                							Tips({ title: '优惠券已领完' })
                							return false;
                						} else if (r == -4) {

                							Tips({ title: '用户领取的优惠券已达上限' })
                							return false;
                						} else if (r == -5) {

                							Tips({ title: '优惠券已失效(删除状态)' })
                							return false;
                						} else if (r == -6) {

                							Tips({ title: '系统繁忙，请重试' })
                							return false;
                						}

                    this.triggerEvent('reflash')
                }
            })
        }
    }
})
