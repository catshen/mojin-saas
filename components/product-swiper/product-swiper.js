
var app = getApp();
Component({
    properties: {
        imgUrls: {
            type: Object,
            value: [],
            observer: function (val) {
                let urls = val.map(item => {
                    return item.uri
                })
                this.setData({
                    urls
                })
            }
        }
    },
    data: {
        circular: true,
        autoplay: true,
        interval: 3000,
        duration: 500,
        currentSwiper: 0,
        urls:[]
    },
    attached: function () {
        
    },
    methods: {
        swiperChange: function (e) {
            this.setData({
                currentSwiper: e.detail.current
            })
        },
        previewImage(e) {
            const {current} = e.currentTarget.dataset
            wx.previewImage({
                current, // 当前显示图片的http链接
                urls: this.data.urls // 需要预览的图片http链接列表
              })
        }
    }
})