
import {refundOPtType} from "../../utils/type"
import { baseURL } from '../../config'
import {getGoodsInfo, applyAfterSale, applyAgain,applyreturn} from "../../api/user"
import { Tips } from '../../utils/util';
import event from '../../utils/events'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hiddenOpt: true,
    optTyle: refundOPtType.ONLY_REFUND,
    goods: {},
    reason:  [
            				'商品信息描述不符',
            				'不想买了',
            				'收货人信息有误',
            				'未按指定时间发货',
            				'其他',
            				'不想买了',
            				'商品质量问题',
            				'收到商品与描述不符',
            				'其他',
            				'系统自动申请'
            			],
    showPop: false,
    reasonIndex: -1,
    fileList: [],
    remark: ""
  },

  showPopup() {
    this.setData({
      showPop: true
    })
  },

  radioChange(e) {
    this.setData({
      reasonIndex: e.detail.value
    })
  },

  hidePopup() {
    this.setData({
      showPop: false
    })
  },

  onlyRefund: function() {
    this.setData({
      optTyle: refundOPtType.ONLY_REFUND,
      hiddenOpt: true
    })
  },

  allRefunds() {
    this.setData({
      optTyle: refundOPtType.REFUNDS,
      hiddenOpt: true
    })
  },

  onSubmit() {
    console.log(this.afterSaleId)
    if(this.afterSaleId) {
      this.$applyAgain()
    } else {
      this.$applyAfterSale();
    }
  },

  // 重新申请
  $applyAgain() {
    let {reason, reasonIndex, optTyle, remark, fileList} = this.data;
    if(!reason[reasonIndex]) {
      return Tips({title: '请选择退款原因'})
    }
    const data = {
      id: this.afterSaleId,
      reason: reason[reasonIndex],
      refund_type: optTyle,
      remark: remark,
      img: fileList.length <= 0 ? '' : fileList[0].url
    }
    applyAgain(data).then(res => {
      if(res.code == 200) {
        event.emit('RESET_LIST');
        Tips({title: res.msg}, {tab: 5, url: '/pages/after_sales_detail/after_sales_detail?afterSaleId=' + res.data})
      }
    })
  },

  onInput(e) {
    this.setData({
        remark: e.detail.value
    })
  },

  $applyAfterSale() {
    let {reason, reasonIndex, optTyle, remark, fileList} = this.data;
    if(!reason[reasonIndex]) {
      return Tips({title: '请选择退款原因'})
    }

var pics='';
    fileList.forEach(function (v) {
        pics=pics+v+','
    })
    const data = {
      orderId: this.orderId,
      reason: reason[reasonIndex],
      refund_type: optTyle,
      desc: remark,
      pics: fileList.length <= 0 ? '' : pics
    }
    if(optTyle==refundOPtType.ONLY_REFUND){
     // 退款
        applyAfterSale(data).then(res => {
          if(res.code == 200) {
          if(res.data==-1){
                     Tips({title: '订单状态错误'});
                     return false;
                    }
            event.emit('RESET_LIST');
            Tips({title: '提交退款成功'});
            wx.redirectTo({
              url: '/pages/after_sales_detail/after_sales_detail?afterSaleId=' + res.data,
            })
          }
        })
    }else{
     // 退货退款
        applyreturn(data).then(res => {
          if(res.code == 200) {
          if(res.data==-1){
           Tips({title: '订单状态错误'});
           return false;
          }else if(res.data==-2){
                           Tips({title: '超过可以退货的时间'});
                           return false;
                          }
            event.emit('RESET_LIST');
            Tips({title: '提交退货退款成功'});
            wx.redirectTo({
              url: '/pages/after_sales_detail/after_sales_detail?afterSaleId=' + res.data,
            })
          }
        })
    }

  },

  afterRead(e) {
    const { file } = e.detail;
    wx.showLoading({
      title: '正在上传中...',
      mask: true
    })
    
    this.uploadFile(file.path).then(res => {
        wx.hideLoading()
        this.setData({
            fileList: [res.data]
        })
    })
  },



  uploadFile(path) {
    return new Promise(resolve => {
        wx.uploadFile({
            url: baseURL + 'minio/oss/uploadToMinio',
            filePath: path,
            name: 'file',
            success: (res) => {
                resolve(res)
            },
        });
    })
  },

  deleteImage(e) {
    let {index} = e.detail;
    let { fileList } = this.data;
    fileList.splice(index, 1)
    this.setData({
      fileList: fileList
    })
  },

  $getGoodsInfo() {
    let {orderId, itemId} = this;
    getGoodsInfo({orderId: orderId}).then(res => {
      if(res.code == 200) {
        this.setData({
          goods: res.data
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let {order_id, item_id, afterSaleId} = options;
    if(options.type==1){
    this.setData({
              optTyle: refundOPtType.ONLY_REFUND
            })
    }else{
    this.setData({
              optTyle: refundOPtType.REFUNDS
            })
    }

    this.orderId = order_id;
    this.itemId = item_id;
    this.afterSaleId = afterSaleId
    this.$getGoodsInfo()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
 onShareAppMessage: async function () {
          let userInfo = wx.getStorageSync("userInfo")
         if(userInfo) {
             return {

                 path: "pages/index/index?recommondCode="+userInfo.selfRecommendCode
             }
         } else {
             return {
                 path: "pages/index/index"
             }
         }
     }
})