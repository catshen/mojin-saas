
import { getCategoryList, getArticleList } from '../../api/store'

Page({

    /**
     * 页面的初始数据
     */
    data: {
        active: 0,

        categoryList: [],
        newsList: [],
        page: 0,

        bannerList: [],
        type: 0
    },
    changeActive(e) {
        let { name } = e.detail
        this.setData({
            active: name,
            page: 0,
            newsList: [],

        })
        setTimeout(() => {
            this.$getArticleList()
        }, 100)

    },
    $getCategoryList() {
        getCategoryList({ type: this.type }).then(res => {
            if(res.code == 200) {
                this.setData({
                    categoryList: res.data,
                })
                if(res.data && res.data.length>0){
                    this.$getArticleList()
                }
            }
        })
    },
    $getArticleList() {
        let { active, page, newsList, status } = this.data

        getArticleList({
            type: this.type,
            id: active ? active : this.data.categoryList[0].id,
        }).then(res => {
            if(res.code == 200) {
                this.setData({
                    newsList:res.data,

                })

            }
        })
    },
    goPage(e) {
        let { id } = e.currentTarget.dataset
        wx.navigateTo({
            url: `/pages/news_details/news_details?id=${id}&type=${this.type}`
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        //分类id
        this.id = options.id
        //type存在则为帮助中心
        this.type = options.type || ''
        this.setData({
            type: this.type
        })
        if (this.type) {
            wx.setNavigationBarTitle({
                title: '帮助中心'
            })
        } else {
            wx.setNavigationBarTitle({
                title: '商城资讯'
            })
        }
        this.$getCategoryList()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.$getArticleList()
    },
    
    onShareAppMessage(res) {
		
	}
})