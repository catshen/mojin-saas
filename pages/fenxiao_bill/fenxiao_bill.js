
import {querycommissionrecords} from "../../api/user"
import {loadingType} from "../../utils/type"
Page({

    /**
     * 页面的初始数据
     */
    data: {
        active: 0,

        lists: [],
        page: 0,
        loadingStatus: loadingType.LOADING
    },

    onChange(e) {
        this.active = e.detail.index-1;

        this.setData({
            active: e.detail.index,
        })
        this.cleanStatus()
        this.$getAccountLog(e.detail.index);
    },

    cleanStatus() {
        // 清理状态
        this.setData({
            page: 0,
            lists: [],
            loadingStatus: loadingType.LOADING
        })
    },

    $getAccountLog(changeType) {

        let {lists, loadingStatus, page }= this.data
        if(loadingStatus == loadingType.FINISHED) return;
        var param ={ pageNum: page};
        if(changeType!=0){
          param.type=changeType-1
        }
        querycommissionrecords(param).then(res => {
            if(res.code == 200) {
                let {totalPages, list} = res.data;
                lists.push(...list);
                this.setData({
                    lists: lists,
                    page: ++ page
                })
                if(totalPages<=page) {
                    this.setData({
                        loadingStatus: loadingType.FINISHED
                    })
                }
                if(lists.length <= 0) {
                    this.setData({
                    loadingStatus: loadingType.EMPTY
                    })
                }
                return;
            } else {
                this.setData({
                    loadingStatus: loadingType.ERROR
                })
            }
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.active = 0;
        this.$getAccountLog(0)
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        this.$getAccountLog(this.active);
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})