
import { getArticleDetail } from '../../api/store'
import wxParse from '../../wxParse/wxParse.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    showLoading: true,
    articleDetail: {}
  },
  $getArticleDetail() {
    getArticleDetail({
      type: this.type,
      id: this.id
    }).then(res => {
      if (res.code == 200) {
        this.setData({
          articleDetail: res.data,
        })
        wxParse.wxParse('content', 'html', res.data.desc, this, 15);
        setTimeout(() => {
          this.setData({
            showLoading: false
          })
        }, 300)
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.type = options.type || ''
    this.id = options.id
    if (this.type) {
      wx.setNavigationBarTitle({
          title: '帮助详情'
      })
  } else {
      wx.setNavigationBarTitle({
          title: '资讯详情'
      })
  }
    this.$getArticleDetail()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})