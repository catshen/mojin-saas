
import { orderTraces } from '../../api/user'
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		shipment: {},
		buy: {},
		delivery: {},
		finish: {},
		order: {},
		take: {},
		isFirstLoading: true
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		this.id = options.id
		this.$orderTraces()
	},
	$orderTraces() {
		orderTraces(this.id).then(res => {
			if (res.code == 200) {

				this.setData({

					order:res.data,

					isFirstLoading: false
				})
			}else {
				setTimeout(() => wx.navigateBack(),1000)
			}
		})
	},
	onCopy() {
		wx.setClipboardData({
			data: this.data.order.invoice_no
		})
	},
	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})