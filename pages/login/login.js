
import { wxLogin } from '../../utils/wxutil'
import { Tips } from '../../utils/util'
const app = getApp()
Page({

    /**
     * 页面的初始数据
     */
    data: {

    },
    getUserInfo(e) {
        if (!e.detail.userInfo) return
        wx.showLoading({
            title: '登录中...',
            mask: true
        })

        let { encryptedData: encrypted_data, iv,userInfo } = e.detail
        wxLogin({
        userInfo,
            encrypted_data,
            iv
        }).then(res => {
            wx.hideLoading()
            if (res.code == 200) {
                app.store.setState({
                    isLogin: true,
                });
               wx.setStorageSync('token', res.data.access_token)
                               wx.setStorageSync('nickname', res.data.member.nickname)
                               wx.setStorageSync('avatar', res.data.member.image)
                                wx.setStorageSync('userInfo', res.data.member)
               
                wx.switchTab({
                                 	url: '/pages/index/index'
                         	})
                         	return;
            }else {
                Tips({title: '登录失败，请稍后再试' })
            }
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})