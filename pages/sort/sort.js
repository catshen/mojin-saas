
import { getCatrgory } from '../../api/store'
import { getRect } from '../../utils/wxutil'
import {userShare} from "../../api/app"
import { navigateTo } from '../../utils/util'
import event from '../../utils/events'
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		asideScroll: 0,
		navActive: '',
		toView: '',
		height: 0,
		leftTop: [],
		rightTop: [],
		asideHeight: '',
		sortList: [],
	},

	goPage(e) {
		let { url, item: {id, name, type} } = e.currentTarget.dataset
		console.log(e)
		wx.navigateTo({
			url: `${url}?id=${id}&name=${name}&type=${type}`,
		})
	},
	init() {
		getRect('.aside').then(res => {
			this.data.asideHeight = res.height
		})
		getRect('.one-item', 'all').then(res => {
			let leftTop = []
			res.forEach((item) => {
				leftTop.push(item.top)
			})
			this.data.leftTop = leftTop
		})
		getRect('.one-item-wrap', 'all').then(async res => {
			let { asideHeight } = this.data
			let rightTop = []
			let headerInfo = await getRect('.header')
			let headerH = headerInfo.height
			let lastheight = res[res.length - 1].height
			res.forEach((item) => {
				rightTop.push({
					id: item.id,
					top: item.top - headerH
				})
			})
			this.data.rightTop = rightTop
			this.setData({
				navActive: rightTop[0].id,
				height: asideHeight - lastheight > 0 ? asideHeight - lastheight : 0
			})
		})
	},
	changeActive(e) {
		let { id } = e.currentTarget.dataset
		this.setData({
			toView: id,
		});
	},
	onScroll(e) {
		let { asideHeight, asideScroll, leftTop, rightTop } = this.data
		let { scrollTop } = e.detail;
		for (let i = rightTop.length - 1; i >= 0; i--) {
			if (scrollTop + 2 >= rightTop[i].top) {
				asideScroll = parseInt(leftTop[i] / asideHeight) * asideHeight
				this.setData({
					navActive: rightTop[i].id,
					asideScroll
				})
				break;
			}
		}
	},
	$getCatrgory() {
		getCatrgory().then((res) => {
			if(res.code == 200){
				this.setData({
					sortList: res.data
				})
				wx.nextTick(()=>{
					this.init()
				});
			}
		})
	},
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		this.$getCatrgory()
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		event.emit('GET_CART_NUM')
	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: async function () {
        let shareRes = await userShare();
        if(shareres.code == 200) {
            return {

                path: "pages/index/index"
            }    
        } else {
            return {
                path: "pages/index/index"
            }
        }
    }
})