
import { getLevelList, getUser } from '../../api/user'
Page({

    /**
     * 页面的初始数据
     */
    data: {
        currentIndex: 0,
        levelList: [],
        userInfo: {},
        growthRule: "",
        privilegeList: []
    },
    bindchange(e) {
        let { current } = e.detail
        let currentLevel = this.data.levelList[current]
        this.setData({
            currentIndex: current,
            privilegeList: currentLevel.privilege_list
        });
    },
    $getLevelList() {
            getUser().then(res => {
    			wx.stopPullDownRefresh({
    				success: (res) => {

    				},
    			})
    			if(res.code == 200) {
    				this.setData({
    					userInfo: res.data,
    				})
    			}else {
    				this.setData({
    					userInfo: {
    						user_money: 0,
    						user_integral: 0,
    						coupon: 0
    					}
    				})
    			}
    		})
        getLevelList().then(res => {

            var level_list = res.data
            var growth_rule='';

            this.setData({

                growthRule: growth_rule,
                levelList: level_list,
                currentIndex: 1,
                privilegeList: this.data.userInfo.customerLevel
            })
        })
    },
    
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.$getLevelList()
        // this.$getUser()
    },

    $getUser() {
        getUser().then(res => {
            if(res.code == 200) {
                this.setData({
                    userInfo: res.data
                })
            }
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {
        return {
            title: 'likeMall',
            path: '/pages/index/index'
          }
    }
})