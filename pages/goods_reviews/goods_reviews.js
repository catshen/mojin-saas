
import { baseURL } from '../../config'
import { goodsComment, getCommentInfo } from '../../api/user'
import { Tips } from '../../utils/util';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        goodsRate: 0,
        descRate: 0,
        serverRate: 0,
        deliveryRate: 0,
        goodsRateDesc: "",
        fileList: [],
        goods: {},
        goodsComment: [],
        storeComment:{},
        comment: ''
    },
    onChange(e) {
        let {type} = e.currentTarget.dataset
        this.setData({
            [type]: e.detail
        })
    },
    goodsRateChange: function (e) {
        let num = e.detail
        let goodsRateDesc = ""
        if (e.detail <= 2) {
            goodsRateDesc = "差评"
        } else if (e.detail == 3) {
            goodsRateDesc = "中评"
        } else {
            goodsRateDesc = "好评"
        }
        this.setData({
            goodsRate: num,
            goodsRateDesc
        })
    },
    onSubmit() {
        let { goodsRate,fileList, comment, deliveryRate, descRate, serverRate, } = this.data

        if(!goodsRate) return Tips({title: '请对商品进行评分'})
        if(!descRate) return Tips({title: '请对描述相符进行评分'})
        if(!serverRate) return Tips({title: '请对服务态度进行评分'})
        if(!deliveryRate) return Tips({title: '请对配送服务进行评分'})
        let commentParams = {};
        let storeComment ={};

              let commentPics = [];
              fileList.forEach(pic => {
                commentPics.push({
                  url: pic
                });
                });

                                commentParams.orderId = this.id;

                                commentParams.comments = new Array();
                                console.log(this.data.goods)
                                for (let i = 0; i < this.data.goods.orderSkus.length; i++) {
                                   let comment = {};
                                           comment.skuId = this.data.goods.orderSkus[i].skuId;
                                            comment.spuId = this.data.goods.orderSkus[i].spuId;
                                           comment.score = goodsRate;
                                           comment.comment = this.data.comment;
                                           comment.isAnonymous = 0;
                                           comment.commentPics = commentPics;
                                           commentParams.comments.push(comment);
                                }
        storeComment.descScore = descRate
        storeComment.sellerScore = serverRate
        storeComment.logisticsScore = deliveryRate
        commentParams.storeComment = storeComment;
        goodsComment(commentParams).then(res => {
            if(res.code == 200) {
                Tips({title: '评价成功'}, { tab: 5, url: '/pages/goods_comment_list/goods_comment_list?type=1' })
            }
        })
    },
    onInput(e) {
        this.setData({
            comment: e.detail.value
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.id = options.id
        this.$getCommentInfo()
    },
    $getCommentInfo() {
        getCommentInfo({
            orderId: this.id
        }).then(res => {
            if(res.code == 200) {

                this.setData({
                    goods: res.data
                })
            }
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    },
    afterRead(e) {
        const { file } = e.detail;
        wx.showLoading({
            title: '正在上传中...',
            mask: true
        })
        let uploadArr = file.map(item => {
            return this.uploadFile(item.path)
        })
        Promise.all(uploadArr).then(res => {
            const {fileList = []} = this.data
            fileList.push(...res)
            wx.hideLoading()
            this.setData({
                fileList
            })
        })
    },
    uploadFile(path) {
        return new Promise(resolve => {
            wx.uploadFile({
                url: baseURL + 'minio/oss/uploadToMinio',
                filePath: path,
                name: 'file',
                success: (res) => {
                    const { fileList = [] } = this.data;
                    resolve(res.data)
                },
                fail: () => {
                    wx.hideLoading()
                    Tips({title: '上传失败，请重新上传'})
                }
            });
        })
    },
    onDelete(e) {
        const { index } = e.detail;
        const { fileList } = this.data;
        fileList.splice(index, 1)
        this.setData({
            fileList
        })
    }
})