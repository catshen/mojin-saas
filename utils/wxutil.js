
import { mnpLogin } from '../api/app'
//用户是否已经授权
export function isAuthorize() {
	return new Promise((resolve, reject) => {
		wx.getSetting({
			success(res) {
				if (res.authSetting['scope.userInfo']) {
					resolve(true)
				} else {
					resolve(false)
				}
			}
		})
	})
}
export function wxgetUserInfo() {
	return new Promise((resolve, reject) => {
		wx.getUserInfo({
			lang: 'zh_CN',
			success(res) {
				resolve(res);
			},
			fail(res) {
				resolve(res);
			}
		})
	});
}

// 获取登录凭证（code）
export function getWxCode() {
	return new Promise((resolve, reject) => {
		wx.login({
			success(res) {
				resolve(res.code)
			},
			fail(res) {
				reject(res)
			}
		})
	})
}


//微信授权登录登录 

export function wxLogin(data) {
	return new Promise((resolve) => {
		getWxCode().then(code => {
			return mnpLogin({ ...data, code,recommondCode:wx.getStorageSync('recommondCode') })
		}).then(res => {
			resolve(res)
		})
	})

}
//授权后自动登录
export function wxAutoLogin() {
console.log('wxAutoLogin')
	return new Promise((resolve) => {
		let code
		return getWxCode().then(res => {
			code = res
			return wxgetUserInfo()
		}).then(res => {
		console.log(res)
			if (!res.userInfo) return
			let { encryptedData: encrypted_data, iv,userInfo } = res
			return mnpLogin({ encrypted_data,iv, code,userInfo ,recommondCode:wx.getStorageSync('recommondCode')})
		}).then(res => {
			resolve(res)
		})
	})

}

// 获取wxml元素
export function getRect(selector, all, context) {
	return new Promise(function (resolve) {
		let qurey = wx
		if(context) {
			qurey = context
		}
		qurey.createSelectorQuery()[all ? 'selectAll' : 'select'](selector)
			.boundingClientRect(function (rect) {
				if (all && Array.isArray(rect) && rect.length) {
					resolve(rect);
				}
				if (!all && rect) {
					resolve(rect);
				}
			})
			.exec();
	});
}

//登录提示弹窗
export function showLoginDialog() {
	wx.showModal({
		title: '提示',
		content: '不授权则无法进行更多操作，点击去授权按钮前往授权',
		confirmText: '去授权',
		success: (res) =>{
			let { confirm } = res
			if (confirm) {
				wx.navigateTo({
					url: '/pages/login/login'
				})
			}
		}
	})
}

// 微信支付
export function wxpay(opt) {
	return new Promise((resolve,reject) => {
		wx.requestPayment({
			timeStamp: opt.time_stamp, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
			nonceStr: opt.nonce_str, // 支付签名随机串，不长于 32 位
			package: opt.package_, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=***）
			signType: opt.sign_type, // 签名方式，默认为'SHA1'，使用新版支付需传入'MD5'
			paySign: opt.pay_sign, // 支付签名
			success: (res) => {
			console.log(res)
					resolve()
			},
			cancel: (res) => {
			console.log(res)
					reject()
			},
			fail: (res) => {
			console.log(res)
					reject()
			}
	});
	})
}