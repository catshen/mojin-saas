
export default {

  //注册事件
  on(name, callback, context = this) {
    if (this[name]) {
      this[name].push({callback, context})
      return
    }
    this[name]= [{callback, context}]
  },
  //移除事件
  remove(name) {
    if(name) {
      delete this[name]
      return
    }
  },

  //触发事件
  emit(name, data) {
    if (this[name]) {
      this[name].forEach(item => {
        item.callback.call(item.context, data)
      })
    }
  }
}

