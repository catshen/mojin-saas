import { storeId } from '../config'
import request from '../utils/request'

//个人中心
export function getUser() {
    return request.get('customerdetail')
}

//用户领取优惠券
export function getCoupon(id) {
    return request.get('receivecoupon', {id:id})
}


// 地址列表
export function getAddressLists() {
    return request.get('querycustomeraddress')
}

// 添加编辑地址
export function editAddress(data) {
    return request.post('updateaddress', data)
}

export function addAddress(data) {
    return request.post('addaddress', data)
}

// 删除地址
export function delAddress(id) {
    return request.post('deletecustomeraddress', {id})
}

// 获取单个地址
export function getOneAddress(id) {
    return request.get('querycustomeraddressbyid', {id})
}

// 获取默认地址
export function getDefaultAddress(id) {
    return request.get('querycustomerDefaultaddress',{})
}

// 设置默认地址
export function setDefaultAddress(id) {
    return request.post('user_address/setDefault', {id})
}

//传省市区字符串判读是否有code
export function hasRegionCode(data) {
    return request.post('user_address/handleRegion',  data)
}

//我的优惠券
export function getMyCoupon(data) {
    return request.get('coupons', data)
}

// 获取商品的收藏列表
export function getCollectGoods(data) {
    return request.get('queryattentions', data)
}

// 商品的增添收藏
export function collectGoods(data) {
    return request.get('addattention', data)
}
// 商品的取消收藏
export function cancelattention(data) {
    return request.get('cancelattention', data)
}
//删除订单
export function delOrder(id) {
    return request.get('order/del', {id})
}

//订单列表
export function getOrderCount(data) {
    return request.get('order/count', data)
}
//订单列表
export function getOrderList(data) {
    return request.get('querycustomerorders', data)
}
//订单详情
export function getOrderDetail(orderId) {
    return request.get('orderdetail', {orderId})
}

//取消订单
export function cancelOrder(orderId) {
    return request.get('cancelorder', {orderId})
}

//物流
export function orderTraces(orderId) {
    return request.get("express", {orderId})
}

//确认收货
export function confirmOrder(orderId) {
    return request.get("receiptorder", {orderId})
}

// 上传图片
export function uploadToMinio(orderId) {
    return request.get("/minio/oss/uploadToMinio", {orderId})
}
// 获取售后列表
export function getAfterSaleList(params) {
    return request.get("backorderlist", params);
}

// 申请退款
export function applyAfterSale(data) {
    return request.get("applyrefund", data)
}
////申请货请求
export function applyreturn(data) {
    return request.post("applyreturn", data)
}
// 获取商品信息
export function getGoodsInfo(params) {
    return request.get("orderdetail", params)
}

// 填写快递信息
export function inputExpressInfo(data) {
    return request.get("fillthelogistics", data)
}

// 撤销申请
export function cancelApply(data) {
    return request.get("deleteBackOrder", data)
}

// 售后详情
export function afterSaleDetail(params) {
    return request.get("querybackdetail", params)
}
export function querybackdetail(params) {
    return request.get("querybackdetail", params)
}



// 重新申请
export function applyAgain(data) {
    return request.post("after_sale/again", data)
}

// 账户明细 积分明细
export function getAccountLog(params) {
    return request.get("predeposits", params)
}
// 佣金明细 type 0收入 1 消费
export function querycommissionrecords(params) {
    return request.get("querycommissionrecords", params)
}
// 佣金提现明细 status 0 申请 1 审核通过 2拒绝 3提现到账
export function querywithdrawrecords(params) {
    return request.get("querywithdrawrecords", params)
}
// 我的分销下线
export function subdistributioncustomer(params) {
    return request.get("subdistributioncustomer", params)
}




// 获取评价信息
export function getCommentInfo(data) {
    return request.get("queryorderforevaluation", data);
}



//商品评价
export function goodsComment(data) {
    return request.post("addorderevaluation", data)
}

// 获取个人详情
export function getUserInfo() {
    return request.get('customerdetail')
}

// 设置个人信息
export function setUserInfo(data) {
    return request.post('user/setInfo', data)
}

// 更换手机号
export function changeUserMobile(data) {
    return request.post('wxapp/bindingPhone', data);
}

//会员中心
export function getLevelList() {
    return request.get('userLevelLists');
}




// 用户钱包
export function getWallet() {
    return request.get("user/myWallet")
}