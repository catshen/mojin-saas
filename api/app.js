
import request from '../utils/request'
import { storeId } from '../config'
// 小程序授权登录
export function mnpLogin(data) {
    return request.post('mpWechatLogin', {...data, storeId: storeId})
}

//预支付接口
export function prepay(data) {
    return request.get('wechatappletpayparams', {...data, storeId: storeId})
}

// 获取签到列表
export function getSignList() {
    return request.get("sign/lists")
}

// 签到
export function userSign() {
    return request.get("sign/sign")
}


// 获取签到规则
export function getSignRule() {
    return request.get("sign/rule")
}

// 获取服务协议
export function getServerProto() {
    return request.get("querybaseinfoset")
}

// 获取隐私政策
export function getPrivatePolicy() {
    return request.get("querybaseinfoset")
}

// 获取售后保障
export function getAfterSaleGuar() {
    return request.get('querybaseinfoset')
}

// 获取首页或个人中心菜单
export function getMenu(data) {
    return request.get('menu/lists', {...data, client: 1})
}

// 获取送优惠券列表
export function getCouponPopList() {
    return request.get("getcouponlist");
}

// 领取注册赠送的优惠券
export function getRegisterCoupon(data) {
    return request.post("coupon/getRegisterCoupon", data)
}

//小程序模板消息订阅
export function getMnpNotice(data) {
    return request.get("subscribe/lists", data);
}

// 用户自定义分享
export function userShare(data) {
    return request.get("index/share", {...data, client: 1})
}